<?php

namespace App\Http\Controllers\Movies;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use Carbon\Carbon;

class Movies extends Controller
{
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    public function add() : string
    {
        $dateNow = Carbon::now();

        $search = tmdb()->searchByDates($dateNow->toDateString(), $dateNow->toDateString());

        //Take only original title and genre from movies
        foreach($search as $s)
        {
            $movies[] = [
              'original_title'   => $s['original_title'],
              'genre_ids'        => json_encode($s['genre_ids'])
            ];

        }
        //add movies list
        if($this->movie->insert($movies)) {
            return "Movies added";
        }

        //if it gets here something went wrong
        return "error";

    }
}
